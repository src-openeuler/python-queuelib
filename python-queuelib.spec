%global _empty_manifest_terminate_build 0
Name:		python-queuelib
Version:	1.7.0
Release:	1
Summary:	Collection of persistent (disk-based) and non-persistent (memory-based) queues
License:	BSD
URL:		https://github.com/scrapy/queuelib
Source0:	https://files.pythonhosted.org/packages/fb/a4/8af5d8ee3526c64a152549a1c7b42896be9fae9a2fda7712883dc09822ac/queuelib-1.7.0.tar.gz
BuildArch:	noarch


%description
Queuelib is a collection of persistent (disk-based) and non-persistent (memory-based) queues for Python.

%package -n python3-queuelib
Summary:	Collection of persistent (disk-based) and non-persistent (memory-based) queues
Provides:	python-queuelib
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-queuelib
Queuelib is a collection of persistent (disk-based) and non-persistent (memory-based) queues for Python.

%package help
Summary:	Development documents and examples for queuelib
Provides:	python3-queuelib-doc
%description help
Development documents and examples for queuelib

%prep
%autosetup -n queuelib-1.7.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-queuelib -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri May 10 2024 xieyanlong <xieyanlong@kylinos.cn> - 1.7.0-1
- Added support for Python 3.10-3.12 and PyPy 3.10

* Tue Aug 02 2022 fushanqing <fushanqing@kylinos.cn> - 1.6.2-1
- update to 1.6.2

* Sun May 23 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
